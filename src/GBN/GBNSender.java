/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GBN;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author frost
 */
public class GBNSender implements Runnable {

    private ArrayList packets;
    private ArrayList window;
    private DatagramSocket socket;
    private InetAddress ip;
    private Random rand;
    private int currentPacket;
    private int port;
    private int probability;
    private int packetsSent = 0;
    private int packetsLost = 0;
    private int prevAck = 0;
    private int expectingRR = 1;
    private final int PACKET_SIZE;
    private final int TIMEOUT;
    private final int WINDOW_SIZE;
    private boolean running;
    private boolean can_send;

    /**
     * The Go-Back-N protocol, Sender side
     * 
     * @param ip The IP of the receiver.
     * @param sendToPort The port of the receiver.
     * @param thisPort The port this machine sends from.
     * @param list The ArrayList of packets.
     * @param PACKET_SIZE The size limit for the packets.
     * @param WINDOW_SIZE The number of packets allowed to be sent at once.
     * @param TIMEOUT The amount of time to track acknowledgements.
     * @param prob The probability of packet loss.
     * @throws SocketException 
     */
    public GBNSender(InetAddress ip, int sendToPort, int thisPort, ArrayList list, int PACKET_SIZE, int WINDOW_SIZE, int TIMEOUT, int prob) throws SocketException {
        currentPacket = 0;
        port = sendToPort;
        this.ip = ip;
        packets = list;
        running = true;
        can_send = true;
        socket = new DatagramSocket(thisPort);
        this.PACKET_SIZE = PACKET_SIZE;
        this.TIMEOUT = TIMEOUT;
        this.WINDOW_SIZE = WINDOW_SIZE;
        probability = prob;
        rand = new Random();
        rand.setSeed(System.currentTimeMillis());
        window = new ArrayList();
    }

    @Override
    public void run() {
        
        //Load the initial window (0, 1, 2, 3)
        for (currentPacket = 0; currentPacket < WINDOW_SIZE; currentPacket++) {
            window.add(packets.get(currentPacket));
        }
        
        //Begin the sending process
        while (running) {
            
            //If the sender is allowed to send (ACK was received), send a burst of packets.
            if (can_send) {
                try {
                    send();
                } catch (IOException ex) {
                    Logger.getLogger(GBNSender.class.getName()).log(Level.SEVERE, null, ex);
                }
                can_send = false;
            }

            //Try to receive acknowledgements for packets that were sent
            byte[] ack = null;
            DatagramPacket ackPacket = null;

            try {
                ackPacket = read();
                ack = ackPacket.getData();
                
                //This is a good ACK, slide the window, sender can send more packets
                if (ackPacket.getLength() == 1) {
                    prevAck = ack[0];
                    slideWindow(ack[0]);
                    can_send = true;
                }
            } catch (IOException ex) {
                Logger.getLogger(GBNSender.class.getName()).log(Level.SEVERE, null, ex);
            }

            //If all packets are sent and the window is clear, end the transmission
            if (currentPacket >= packets.size() && window.size() == 0) {
                try {
                    eot();
                    return;
                } catch (IOException ex) {
                    Logger.getLogger(GBNSender.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            //If there is no data or it is not the expected acknowledgement, ignore the packet
            if (ack == null || ack[0] == prevAck) {
                //Resend the same packet until ACK is received
                continue;
            }
        }
    }

    /**
     * Sends a burst of packets from the current window.
     * @throws IOException 
     */
    private void send() throws IOException {
        for (int i = 0; i < window.size(); i++) {
            byte[] packet = (byte[]) window.get(i);
            DatagramPacket dp = new DatagramPacket(packet, packet.length, ip, port);

            //Determine if a packet is lost or not
            if (rand.nextInt(100) > probability) {
                socket.send(dp);
                packetsSent++;
            } else {
                packetsLost++;
            }
        }
        can_send = false;
    }

    /**
     * Try to read an ACK from the socket.
     * @return Return the ACK that was received.
     * @throws IOException 
     */
    private DatagramPacket read() throws IOException {
        byte[] r = new byte[PACKET_SIZE];
        DatagramPacket recPack = new DatagramPacket(r, r.length);
        try {
            socket.setSoTimeout(TIMEOUT);
            socket.receive(recPack);
        } catch (SocketTimeoutException ste) {
            //Timeout on oldest packet expired, resend all packets in window
            send();
        }
        return recPack;
    }

    /**
     * Slide the window to track the next available packets for sending.
     * 
     * @param up The sequence number of the acknowledgement received.
     */
    private void slideWindow(byte up) {
        int upperBound = 0;

        //Determine the interval to slide the window
        if (up < ((byte[]) window.get(0))[0]) {
            //The ACK received is less than the oldest packet in window, increase the ACK by 8
            upperBound = up + 8;
        } else {
            //The ACK received is greater than the oldest packet in the window
            upperBound = up;
        }

        //Calculate the sliding interval
        int lowerBound = (int) ((byte[]) window.get(0))[0];
        int size = (upperBound - lowerBound);

        //Slide the window based on the interval, deletion process
        if (size <= window.size()) {
            if (!window.isEmpty()) {
                for (int i = 0; i < size; i++) {
                    window.remove(0);
                }
            }
        }

        //Slide the window based on the interval, insertion process
        for (int j = window.size(); j < WINDOW_SIZE; j++) {
            if (!(currentPacket >= packets.size())) {
                window.add(packets.get(currentPacket));
                currentPacket++;
            }
        }
    }

    /**
     * Sends the End of Transmission packet.
     * @throws IOException 
     */
    private void eot() throws IOException {
        byte[] eot = new byte[1];
        eot[0] = -1;
        DatagramPacket dp = new DatagramPacket(eot, eot.length, ip, port);
        socket.send(dp);
        //Print statistical data
        System.out.println("Number of packets sent: " + packetsSent);
        System.out.println("Number of packets lost: " + packetsLost);
    }
}
