/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GBN;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import static java.lang.Thread.yield;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author frost
 */
public class GBNReceiver implements Runnable {

    private ArrayList packets;
    private DatagramSocket socket;
    private int port;
    private int expecting = 0;
    private boolean running;
    private final int PACKET_SIZE;
    private final int WINDOW_SIZE;
    private PrintWriter pw;

    /**
     * The Go-Back-N protocol, Receiver side
     * 
     * @param list The list of packets.
     * @param receiverPort The port to listen to.
     * @param PACKET_SIZE The size of each packet.
     * @param WINDOW_SIZE The window size for sequence number calculation.
     * @throws SocketException
     * @throws FileNotFoundException 
     */
    public GBNReceiver(ArrayList list, int receiverPort, int PACKET_SIZE, int WINDOW_SIZE) throws SocketException, FileNotFoundException {
        packets = list;
        running = true;
        socket = new DatagramSocket(receiverPort);
        this.PACKET_SIZE = PACKET_SIZE;
        this.WINDOW_SIZE = WINDOW_SIZE;
        port = receiverPort;
        pw = new PrintWriter(new File("../COSC635_P2_DataReceived.txt"));
    }

    @Override
    public void run() {
        System.out.println("Accepting data packets on port " + port);

        //Listen for traffic on the port
        while (running) {
            byte[] receivedPacket = new byte[PACKET_SIZE];
            DatagramPacket rP = new DatagramPacket(receivedPacket, receivedPacket.length);
            
            //Listen to the port for 2ms, track which packets have arrived in order
            long startRead = System.currentTimeMillis();
            while (System.currentTimeMillis() - startRead < 2) {
                try {
                    socket.receive(rP);

                    //If eot packet, quit
                    if (rP.getData()[0] == -1) {
                        running = false;
                        break;
                    }

                    //If the packet is the one we are waiting for, print and wait for the next
                    if (rP.getData()[0] == expecting) {
                        expecting = ((expecting + 1) % (WINDOW_SIZE * 2));
                        byte[] accepted = rP.getData();
                        try {
                            print(accepted);
                        } catch (UnsupportedEncodingException ex) {
                            Logger.getLogger(GBNReceiver.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                } catch (IOException ex) {
                    Logger.getLogger(GBNReceiver.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            byte[] payload = rP.getData();

            //Secondary check for packets outside of timeout
            if (payload[0] == -1) {
                running = false;
                System.out.println("Shutting down receiver");
            } else if (payload[0] == expecting) {
                expecting = ((expecting + 1) % (WINDOW_SIZE * 2));
                try {
                    print(payload);
                } catch (UnsupportedEncodingException ex) {
                    Logger.getLogger(GBNReceiver.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(GBNReceiver.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            //Send a request for the currently expected packet
            try {
                sendAck(expecting, rP);
            } catch (IOException ex) {
                Logger.getLogger(GBNReceiver.class.getName()).log(Level.SEVERE, null, ex);
            }

            //Final check for shutdown
            if (rP.getData()[0] == -1) {
                running = false;
                //break;
            }

            yield();
        }
        
        //Finish printing the received file
        pw.flush();
        pw.close();
    }

    /**
     * Send a request for the currently expected packet.
     * 
     * @param id The id of the packet received.
     * @param rec The entire packet that was received.
     * @throws IOException 
     */
    private void sendAck(int id, DatagramPacket rec) throws IOException {
        byte[] ack = new byte[1];
        ack[0] = (byte) ((id) % (WINDOW_SIZE * 2));
        
        //If a recipient has not yet been determined, return
        if (rec.getPort() == -1) {
            return;
        }
        
        //Send the request
        DatagramPacket ackPack = new DatagramPacket(ack, ack.length, rec.getAddress(), rec.getPort());
        socket.send(ackPack);
    }

    /**
     * Print the packet that was received.
     * 
     * @param payload The data packet being printed
     * @throws UnsupportedEncodingException 
     */
    private void print(byte[] payload) throws UnsupportedEncodingException {
        byte[] printer = new byte[PACKET_SIZE - 1];
        System.arraycopy(payload, 1, printer, 0, PACKET_SIZE - 1);
        String out = new String(printer, "UTF-8");
        pw.print(out);
    }
}
