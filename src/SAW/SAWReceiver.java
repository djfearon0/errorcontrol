/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SAW;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import static java.lang.Thread.yield;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author frost
 */
public class SAWReceiver implements Runnable {

    private ArrayList packets;
    private DatagramSocket socket;
    private int port;
    private int expecting = 0;
    private boolean running;
    private final int PACKET_SIZE;
    private PrintWriter pw;

    /**
     * The Stop-and-Wait protocol, Receiver side
     * 
     * @param list The list of packets.
     * @param receiverPort The port to listen to.
     * @param PACKET_SIZE The size of each packet.
     * @throws SocketException
     * @throws FileNotFoundException 
     */
    public SAWReceiver(ArrayList list, int receiverPort, int PACKET_SIZE) throws SocketException, FileNotFoundException {
        packets = list;
        running = true;
        socket = new DatagramSocket(receiverPort);
        this.PACKET_SIZE = PACKET_SIZE;
        port = receiverPort;
        pw = new PrintWriter(new File("../COSC635_P2_DataReceived.txt"));
    }

    @Override
    public void run() {
        System.out.println("Accepting data packets on port " + port);
        while (running) {
            byte[] receivedPacket = new byte[PACKET_SIZE];
            DatagramPacket rP = new DatagramPacket(receivedPacket, receivedPacket.length);

            //Listen for packets
            try {
                socket.receive(rP);
            } catch (IOException ex) {
                Logger.getLogger(SAWReceiver.class.getName()).log(Level.SEVERE, null, ex);
            }

            byte[] payload = rP.getData();

            //If eot, quit
            if (payload[0] == -1) {
                running = false;
            } 
            //Check if packet is what is expected, increment, and print
            else if (payload[0] == expecting) {
                expecting ^= 1;
                try {
                    print(payload);
                } catch (UnsupportedEncodingException ex) {
                    Logger.getLogger(SAWReceiver.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            //Acknowledge the received packet
            try {
                sendAck(payload[0], rP);
            } catch (IOException ex) {
                Logger.getLogger(SAWReceiver.class.getName()).log(Level.SEVERE, null, ex);
            }

            yield();
        }
        //Finish printing the received file
        pw.flush();
        pw.close();
    }

    /**
     * Send a request for the currently expected packet.
     * 
     * @param id The id of the packet received.
     * @param rec The entire packet that was received.
     * @throws IOException
     */
    private void sendAck(byte id, DatagramPacket rec) throws IOException {
        byte[] ack = new byte[1];
        id ^= 1;
        ack[0] = id;
        DatagramPacket ackPack = new DatagramPacket(ack, ack.length, rec.getAddress(), rec.getPort());
        socket.send(ackPack);
    }

    /**
     * Print the packet that was received.
     * 
     * @param payload The data packet being printed.
     * @throws UnsupportedEncodingException 
     */
    private void print(byte[] payload) throws UnsupportedEncodingException {
        byte[] printer = new byte[PACKET_SIZE - 1];
        System.arraycopy(payload, 1, printer, 0, PACKET_SIZE - 1);
        String out = new String(printer, "UTF-8");
        pw.print(out);
    }
}
