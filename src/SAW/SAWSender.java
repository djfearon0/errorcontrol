/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SAW;

import java.io.IOException;
import static java.lang.Thread.yield;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author frost
 */
public class SAWSender implements Runnable {

    private ArrayList packets;
    private DatagramSocket socket;
    private InetAddress ip;
    private Random rand;
    private int currentPacket;
    private int port;
    private int probability;
    private int packetsSent = 0;
    private int packetsLost = 0;
    private int prevAck = -1;
    private final int PACKET_SIZE;
    private final int TIMEOUT;
    private boolean running;
    private boolean can_send;

    /**
     * The Stop-and-Wait protocol, Sender side
     * 
     * @param ip The IP of the receiver.
     * @param sendToPort The port of the receiver.
     * @param thisPort The port this machine sends from.
     * @param list The ArrayList of packets.
     * @param PACKET_SIZE The size limit for the packets.
     * @param TIMEOUT The amount of time to track acknowledgements.
     * @param prob The probability of packet loss.
     * @throws SocketException
     */
    public SAWSender(InetAddress ip, int sendToPort, int thisPort, ArrayList list, int PACKET_SIZE, int TIMEOUT, int prob) throws SocketException {
        currentPacket = 0;
        port = sendToPort;
        this.ip = ip;
        packets = list;
        running = true;
        can_send = true;
        socket = new DatagramSocket(thisPort);
        this.PACKET_SIZE = PACKET_SIZE;
        this.TIMEOUT = TIMEOUT;
        probability = prob;
        rand = new Random();
        rand.setSeed(System.currentTimeMillis());
    }

    @Override
    public void run() {
        while (running) {
            if (currentPacket == packets.size()) {
                try {
                    eot();
                    return;
                } catch (IOException ex) {
                    Logger.getLogger(SAWSender.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            if (can_send) {
                try {
                    send();
                } catch (IOException ex) {
                    Logger.getLogger(SAWSender.class.getName()).log(Level.SEVERE, null, ex);
                }
                can_send = false;
            }

            byte[] ack = null;

            try {
                ack = read();
            } catch (IOException ex) {
                Logger.getLogger(SAWSender.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (ack == null || ack[0] == prevAck) {
                //Resend the same packet until ACK is received
                continue;
            }
            else {
                prevAck = ack[0];
                can_send = true;
                yield();
            }
        }
    }

    /**
     * Sends the current packet.
     * @throws IOException 
     */
    private void send() throws IOException {
        byte[] packet = (byte[]) packets.get(currentPacket);
        DatagramPacket dp = new DatagramPacket(packet, packet.length, ip, port);

        //Determine if the packet is lost or not
        if (rand.nextInt(100) > probability) {
            socket.send(dp);
            packetsSent++;
        } else {
            packetsLost++;
        }
    }

    /**
     * Check for acknowledgements.
     * @return A packet with the acknowledgement.
     * @throws IOException 
     */
    private byte[] read() throws IOException {
        byte[] r = new byte[PACKET_SIZE];
        DatagramPacket recPack = new DatagramPacket(r, r.length);
        //Check for ACK. If none received, resend the current packet
        try {
            socket.setSoTimeout(TIMEOUT);
            socket.receive(recPack);
        } catch (SocketTimeoutException ste) {
            //Backtrack to the lost packet and resend
            currentPacket--;
            can_send = true;
            packetsSent++;
        }
        //Increment to the next packet
        currentPacket++;
        byte[] rec = recPack.getData();
        return rec;
    }

    /**
     * Sends the End of Transmission packet.
     * @throws IOException 
     */
    private void eot() throws IOException {
        byte[] eot = new byte[1];
        eot[0] = -1;
        DatagramPacket dp = new DatagramPacket(eot, eot.length, ip, port);
        socket.send(dp);
        System.out.println("Number of packets sent: " + packetsSent);
        System.out.println("Number of packets lost: " + packetsLost);
    }

}
