/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SAW;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 *
 * @author frost
 */
public class StopAndWait {

    private ArrayList packets = new ArrayList();
    private ArrayList received = new ArrayList();
    DatagramSocket sender;
    private File f;
    private InetAddress addr;
    private Random rand;
    private byte[] buf;
    private AtomicBoolean ack_flag = new AtomicBoolean(true);
    private int p;
    private int sP;
    private int rP;
    private int probability;
    private static final int PACKET_SIZE = 1024;
    private static final int TIMEOUT = 200;

    /**
     * Creates a new Sender and Receiver object for Stop-and-Wait transfer.
     * 
     * @param file The input file that is being sent.
     * @param ip The IP Address of the receiving computer.
     * @param port The port that this machine is sending from.
     * @param sendPort The port that the receiver is reading from.
     * @param recPort The port that this machine is receiving from.
     * @param probability The user-given probability of packet loss.
     */
    public StopAndWait(File file, InetAddress ip, int port, int sendPort, int recPort, int probability) {
        f = file;
        addr = ip;
        p = port;
        sP = sendPort;
        rP = recPort;
        buf = new byte[PACKET_SIZE - 1];
        rand = new Random();
        this.probability = probability;
    }

    /**
     * Launch the protocol and transfer.
     * @throws SocketException
     * @throws IOException
     * @throws InterruptedException 
     */
    public void run() throws SocketException, IOException, InterruptedException {
        sender = new DatagramSocket();
        
        SAWReceiver r = new SAWReceiver(packets, rP, PACKET_SIZE);
        SAWSender s = new SAWSender(addr, sP, p, packets, PACKET_SIZE, TIMEOUT, probability);
        
        //Starts the receiver
        Thread rt = new Thread(r);
        rt.start();

        //Reads in the file from storage into a series of byte arrays.
        readFile();

        Scanner in = new Scanner(System.in);
        System.out.println("Press enter to start sending.");

        //Starts the sender
        Thread st = new Thread(s);
        
        //Halts the process, waits to send until user confirms by hitting enter.
        in.nextLine();
        long start = System.currentTimeMillis();
        st.start();
        
        

        //Join threads after transfer is complete.
        st.join();
        rt.join();
        long end = System.currentTimeMillis();
        
        //Print the total time for full-duplex transfers
        long total = end - start;
        System.out.println("The total transfer time was " + total + " milliseconds.");

        System.exit(0);
    }

    /**
     * Reads in the file that is being transferred to the recipient.
     * 
     * @throws FileNotFoundException
     * @throws IOException 
     */
    private void readFile() throws FileNotFoundException, IOException {
        FileInputStream in = new FileInputStream(f);
        int read = 0;
        byte id = 0;
        byte[] buffer = new byte[PACKET_SIZE];
        buffer[0] = id;

        while ((read = in.read(buf)) != -1) {
            System.arraycopy(buf, 0, buffer, 1, buf.length);
            packets.add(buffer);
            buf = new byte[PACKET_SIZE - 1];
            buffer = new byte[PACKET_SIZE];
            id ^= 1;
            buffer[0] = id;
        }

        in.close();
    }
}
