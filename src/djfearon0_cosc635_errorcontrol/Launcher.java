/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package djfearon0_cosc635_errorcontrol;

import GBN.GoBackN;
import SAW.StopAndWait;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 *
 * @author frost
 */
public class Launcher {

    /**
     * 
     * 
     * @throws UnknownHostException
     * @throws IOException 
     * @throws java.net.SocketException 
     * @throws java.lang.InterruptedException 
     */
    public void launch() throws UnknownHostException, IOException, SocketException, InterruptedException {
        File file = new File("../COSC635_P2_DataSent.txt");
        Scanner input = new Scanner(System.in);

        System.out.println("Your IP Address is: " + InetAddress.getLocalHost().getHostAddress() + "\n");
        
        boolean goodIP = false;
        InetAddress ip = null;
        while(!goodIP){
            System.out.print("Please enter a target IP Address: ");
            String ipCandidate = input.nextLine();
            if(checkIP(ipCandidate)){
                ip = InetAddress.getByName(ipCandidate);
                goodIP = true;
            } else{
                System.out.println("Please enter a valid IP Address.\n");
            }
        }
        

        boolean goodPort = false;
        boolean goodProtocol = false;
        
        int socket = -1;
        int protocol = -1;

        //Get a port number from the user. If it is out of range, loop until good.
        while (!goodPort) {
            System.out.print("Please enter the port you would like to send packets from: ");
            socket = input.nextInt();
            if (socket < 1024 || socket > 9999) {
                System.out.println("Socket is out of range. Plese enter a port between 1024 and 9999");
            } else {
                goodPort = true;
            }
        }
        
        goodPort = false;
        int sendPort = -1;
        //Get a port number from the user. If it is out of range, loop until good.
        while (!goodPort) {
            System.out.print("Please enter the port that the recipient is accepting packets on: ");
            sendPort = input.nextInt();
            if (sendPort < 1024 || sendPort > 9999) {
                System.out.println("Socket is out of range. Plese enter a port between 1024 and 9999");
            } else {
                goodPort = true;
            }
        }
        
        goodPort = false;
        int receiverPort = -1;
        //Get a port number from the user. If it is out of range, loop until good.
        while (!goodPort) {
            System.out.print("Please enter the port you would like to receive packets on: ");
            receiverPort = input.nextInt();
            if (receiverPort < 1024 || receiverPort > 9999) {
                System.out.println("Socket is out of range. Plese enter a port between 1024 and 9999");
            } else {
                goodPort = true;
            }
        }
        
        int probability = 0;
        boolean goodProb =false;
        //Get packet loss probability. If it is out of range, loop until good.
        while(!goodProb){
            System.out.print("Please enter the probability of packet loss: ");
            probability = input.nextInt();
            if(probability >= 0 && probability <= 99){
                goodProb = true;
            } else {
                System.out.println("Please enter a vlaue from 0 to 99");
            }
        }

        //Get protocol choice from user. If number does not match a protocol, loop until good.
        while (!goodProtocol) {
            System.out.print("Please select a protocol:\n"
                    + "0: Stop-and-Wait\n"
                    + "1: Go-Back-N\n\n"
                    + "Choice: ");
            protocol = input.nextInt();
            if (protocol != 0 && protocol != 1) {
                System.out.println("Please choose one of the protocols listed.");
            }
            else{
                goodProtocol = true;
            }
        }
        
        //Determine which protocol to use based on user's choice.
        switch(protocol){
            case 0 : SAW(file, ip, socket, sendPort, receiverPort, probability); break;
            case 1 : GBN(file, ip, socket, sendPort, receiverPort, probability); break;
        }
    }
    
    /**
     * Starts the Stop-and-Wait protocol.
     * 
     * @param file The file being sent.
     * @param ip The IP address of the receiver.
     * @param socket The socket packets are sent from.
     * @param sendPort The socket of the recipient.
     * @param recPort The socket packets are received on.
     * @param prob The probability of packet loss.
     * @throws IOException
     * @throws SocketException
     * @throws InterruptedException 
     */
    private void SAW(File file, InetAddress ip, int socket, int sendPort, int recPort, int prob) throws IOException, SocketException, InterruptedException{
        StopAndWait saw = new StopAndWait(file, ip, socket, sendPort, recPort, prob);
        System.out.println("Running the Stop-and-Wait protocol");
        saw.run();
    }
    
    /**
     * Starts the Go-Back-N protocol.
     * 
     * @param file The file being sent.
     * @param ip The IP address of the receiver.
     * @param socket The socket packets are sent from.
     * @param sendPort The socket of the recipient.
     * @param recPort The socket packets are received on.
     * @param prob The probability of packet loss.
     * @throws IOException
     * @throws SocketException
     * @throws InterruptedException 
     */
    private void GBN(File file, InetAddress ip, int socket, int sendPort, int recPort, int prob) throws IOException, SocketException, InterruptedException{
        GoBackN gbn = new GoBackN(file, ip, socket, sendPort, recPort, prob);
        System.out.println("Running the Go-Back-N protocol");
        gbn.run();
    }
    
    /**
     * Checks to see if a given IP matches the proper format.
     * 
     * @param ip The string given by the user.
     * @return Returns a boolean based on if the given string matches IP address format.
     */
    private boolean checkIP(String ip){
        String[] checker = ip.split("\\.");
        int i = 0;
        for(i = 0; i < checker.length; i++){
            int part = Integer.parseInt(checker[0]);
            if(part >= 0 && part < 256){
            } else{
                return false;
            }
        }
        return i == 4;
    }
}
